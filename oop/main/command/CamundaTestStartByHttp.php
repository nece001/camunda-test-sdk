<?php

namespace org\camunda\php\sdk\command;

use Illuminate\Console\Command;
use org\camunda\php\sdk\logic\Camunda\ProcessInstance;
use org\camunda\php\sdk\logic\Camunda\Daemon;
use Illuminate\Support\Facades\Redis;
use App\Logic\Flow\CaseLogic;
use Exception;

class CamundaTestStartByHttp extends Command
{
    private $messageKey = '';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'camunda:TestStartByHttp {id} {message_key}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '启动canunda的测试用例和等待结果输出';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(){
        $id = $this->argument('id');
        $this->messageKey = $this->argument('message_key');
        $case_data = $this->getCaseData($id);
        try {
            if ($case_data) {
                $definitionKey = $case_data->definition_key;
                $modelFilename = isset($case_data->model_filename) ? $case_data->model_filename : $definitionKey.'.bpmn';
                $this->publishStart($modelFilename);

                foreach ($case_data->cases as $key=>$case) {
                    $workflowId = $this->startWorkflow($definitionKey, $key, $case);
                    $this->runWorkflow($workflowId, $definitionKey, $key, $case->description, $case->expect);
                }

                $this->publishFinish('finish');
            }
        }catch(Exception $e){
            $this->publishFinish($e->getMessage());
        }
    }

    public function getCaseData($id){
        $caseL = new CaseLogic();
        $data = $caseL->getFlowFileCaseDataById($id);
        if($data){
            return json_decode(json_encode($data));
        }
        return null;
    }

    public function publish($message)
    {
        $this->output($message);
        Redis::rpush($this->messageKey, json_encode($message));
    }

    public function publishStart($modelFilename)
    {
        $this->testId = uniqid();
        $message = ['BPMN模型：'.$modelFilename, '用例执行结果：', PHP_EOL];
        $message['type'] = 'start';
        $message['test_id'] = $this->testId;
        $this->publish($message);
        return $this->testId;
    }

    public function publishMessage($definitionKey, $businessKey, $description, $expect, $result)
    {
        $message = [
            'type'=>'bpmn',
            'test_id'=>$this->testId,
            'definition_key'=>$definitionKey,
            'businessKey'=>$businessKey,
            'description'=>$description,
            'case_expect'=>$expect,
            'output'=>$result
        ];
        $this->publish($message);
    }

    public function publishFinish($message)
    {
        $message = ['type'=>'finish', 'message'=>$message, 'test_id'=>$this->testId];
        $this->publish($message);
    }

    public function output($message)
    {
        print_r($message);
        echo PHP_EOL;
    }

    private $workflowIds = [];
    private $testId;

    public function startWorkflow($definitionKey, $businessKey, $case)
    {
        $processInstance = new ProcessInstance();
        if (isset($case->start_message)) {
            $instance = $processInstance->startProcessByMessage($case->start_message, $businessKey);
        } else {
            $instance = $processInstance->startProcess($definitionKey, $businessKey, $case->variables);
        }

        $id = $instance->getId();
        $this->workflowIds[] = $id;
        return $id;
    }

    public function runWorkflow($processId, $definitionKey, $businessKey, $description, $expect)
    {
        $daemon = new Daemon();
        $offset = 0;
        $completed = false;
        $result = [];
        while (!$completed) {
            $completed = $daemon->processInstanceIsCompletedById($processId);
            $activityInstances  = $daemon->fetchHistoryActivityInstances($processId, $offset);
            if($activityInstances){
                foreach ($activityInstances as $aci) {
                    $offset++;
                    $result[] = $aci->getActivityName();
                }
            }else{
                sleep(1);
            }
        }

        //$activityNames  = $daemon->fetchHistoryActivityInstances($processId);
        $this->publishMessage($definitionKey, $businessKey, $description, $expect, $result);
    }
}
