<?php

namespace org\camunda\php\sdk\command;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Artisan;

class CamundaTestListener extends Command
{
    public static $results = [];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'camunda:TestListener';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '对比运行结果';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
         Redis::psubscribe(['camunda_start_process'], function ($id, $channel) {
             $exitCode = Artisan::call('camunda:TestStartByHttp', ['id'=>$id]);
         });
    }
}
