<?php

namespace org\camunda\php\sdk\command;

use Illuminate\Console\Command;
use org\camunda\php\sdk\logic\Camunda\ProcessInstance;
use org\camunda\php\sdk\logic\Camunda\Daemon;
use Illuminate\Support\Facades\Redis;

class CamundaTestStart extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'camunda:TestStart {test_data_filename}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '启动canunda的测试用例和等待结果输出';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $processInstanceIds = [];
        $cases_path = public_path('test/cases');
        $test_data_filename = $cases_path.'/'.trim($this->argument('test_data_filename'), "\\/");
        $basename = basename($test_data_filename);
        $case_data = json_decode(file_get_contents($test_data_filename));
        $reports = [];
        if($case_data){
            $test_name = $case_data->test_name;
            $definition_key = $case_data->definition_key;
            $model_filename = isset($case_data->model_filename) ? $case_data->model_filename : $definition_key.'.bpmn';
            //print_r($data);


            $testId = uniqid();
            $filename = $test_data_filename.'_result.txt';
            $logs = [$test_name, '用例文件：'.$basename, 'BPMN模型：'.$model_filename, '用例执行结果：', PHP_EOL];
            $reports = $logs;

            $processInstance = new ProcessInstance();
            $daemon = new Daemon();
            foreach($case_data->cases as $key=>$case){
                try {
                    if(isset($case->start_message)){
                        $instance = $processInstance->startProcessByMessage($case->start_message, $key);
                    }else{
                        $instance = $processInstance->startProcess($definition_key, $key, $case->variables);
                    }
                    $id = $instance->getId();
                    $processInstanceIds[$id] = $id;

                    echo '执行用例：'.$case->explan, PHP_EOL;
                    echo '-----------------------------------', PHP_EOL;
                    echo 'processInstanceId: '.$id, PHP_EOL;

                    $offset = 0;
                    $completed = false;
                    while(!$completed){
                        $completed = $daemon->processInstanceIsCompletedById($id);
                        $activityInstances  = $daemon->fetchHistoryActivityInstances($id, $offset);
                        foreach($activityInstances as $aci){
                            $offset++;
                            echo $offset.":".$aci->getActivityName(), PHP_EOL;
                        }
                        sleep(1);
                    }
                    echo PHP_EOL, '用例执行完毕。', PHP_EOL;
                    echo '-----------------------------------', PHP_EOL.PHP_EOL;

                }catch(\Exception $e){
                    echo '-----------------------------------', PHP_EOL.PHP_EOL;
                    echo $case->explan, PHP_EOL;
                    echo $e->getMessage(), PHP_EOL;
                    echo '-----------------------------------', PHP_EOL.PHP_EOL;
                }
            }


            $message = $logs;
            $message['type'] = 'start';
            $message['test_id'] = $testId;
            $this->publish($message);
            echo implode(PHP_EOL, $logs);

            
            while($processInstanceIds){
                //echo 'next...'.PHP_EOL;
                $completeds = $daemon->fetchCompletedProcessInstancesByIds($processInstanceIds);
                //print_r($completeds);

                foreach($completeds as $instance){
                    $processId = $instance->getId();
                    $businessKey = $instance->getBusinessKey();
                    $state = $instance->state;
                    $case = $case_data->cases->$businessKey;
                    $case_expect = $case->expect;
                    //echo PHP_EOL.PHP_EOL.PHP_EOL;
                    //echo $businessKey;
                    //print_r($case);

                    $logs[] = 'processId: '.$processId.' '. $state;
                    $logs[] = 'businessKey: '.$businessKey;
                    $logs[] = '说明：'.$case->explan;
                    $logs[] = '期望: '.implode(', ', $case_expect);
                    $match_count = 0;
                    $result = [];

                    $activityInstances  = $daemon->fetchHistoryActivityInstances($processId);
                    foreach($activityInstances as $activity){
                        //print_r($activity);
                        $activityName = $activity->getActivityName();
                        $activityName = $activityName ?? $activity->getActivityId();
                        $result[] = $activityName;
                        
                        if(in_array($activityName, $case_expect)){
                            $match_count++;
                            //echo $activityName, "\t";
                        }
                    }

                    $ok = $match_count > 0 && $match_count == count($case_expect) ? 'ok' : 'fail';
                    $logs[] = '执行流程：'.implode(', ', $result);
                    $logs[] = $ok;
                    $logs[] = '';

                    $report = $case->explan."\t[".$ok."]";
                    if($ok == 'fail'){
                        $this->error($report);
                    }else{
                        $this->info($report);
                    }
                    
                    //echo $report. PHP_EOL;
                    $reports[] = $report;

                    $message = [
                        'type'=>'bpmn',
                        'test_id'=>$testId,
                        'definition_key'=>$definition_key,
                        'businessKey'=>$businessKey,
                        'explan'=>$case->explan,
                        'case_expect'=>$case_expect,
                        'output'=>$result
                    ];
                    $this->publish($message);

                    //echo PHP_EOL;
                    unset($processInstanceIds[$processId]);
                }

                sleep(5);
            }

            $log = implode(PHP_EOL, $reports);
            //file_put_contents($filename, $log);
            $message = ['type'=>'finish', 'test_id'=>$testId];
            $this->publish($message);
            echo 'finish.'.PHP_EOL;
        }
    }

    public function publish($message){
        Redis::publish('camunda_bpmn', json_encode($message));
    }
}
