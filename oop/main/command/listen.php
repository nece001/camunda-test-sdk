<?php

namespace org\camunda\php\sdk\command;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;

class listen extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'camunda:listen';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        /**/
        Redis::psubscribe(['*'], function ($message, $channel) {
            echo $message, ' = ', $channel, PHP_EOL;
        });
    }
}
