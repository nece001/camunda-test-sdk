<?php

namespace org\camunda\php\sdk\command;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Storage;
use App\Logic\Flow\TestingRecordLogic;

class CamundaTestAuditByHttp extends Command
{
    public static $results = [];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'camunda:TestAuditByHttp {id} {message_key}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '对比运行结果';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        set_time_limit(0);
        $id = $this->argument('id');
        $message_key = $this->argument('message_key');
        //echo $message_key;
        $flag = true;
        $i=0;
        while($flag && $i<100){
            $message = Redis::lpop($message_key);
            //echo $message, PHP_EOL;
            if ($message) {
                $flag = $this->checkMessage($message);
            }
            sleep(1);
            $i++;
        }
    }

    public function println($message){
        echo '<p>'.str_replace("\r\n", "<br>", $message).'</p>';
    }

    private function checkMessage($message){
        $data = json_decode($message, true);
        $type = $data['type'];
        $test_id = $data['test_id'];
        unset($data['type'], $data['test_id']);
        if($type == 'start'){
            CamundaTestAudit::$results[$test_id] = $data;
            $this->println(implode(PHP_EOL, $data));
        }elseif($type == 'bpmn'){
            $explan = $data['description'];
            $case_id = $data['businessKey'];
            $case_expect = $data['case_expect'];
            $output = $data['output'];
            $match_count = 0;
            foreach($output as $except){
                if(in_array($except, $case_expect)){
                    $match_count++;
                }
            }

            $is_ok = count($case_expect) == $match_count;
            $result = $is_ok ? 'success' : 'fail';
            $result_message = $explan."\t".$result;
            CamundaTestAudit::$results[$test_id][] = $result_message;
            if($is_ok){
                $this->info($result_message);
            }else{
                $this->error($result_message);
            }
            $this->println($result_message);

            $recordM = new TestingRecordLogic();
            $recordM->save($case_id, $explan, $result);
        }elseif($type == 'finish'){
            // 保存文件
            $data = CamundaTestAudit::$results[$test_id];
            $content = implode(PHP_EOL, $data);
            unset(CamundaTestAudit::$results[$test_id]);

            //$path = date('Ymd');

            // /storage/app/public
            //Storage::disk('public')->put($path.'/'.$test_id.'.txt', $content);
            $this->info($test_id.' finish.');
            return false;
        }
        return true;
    }
}
