<?php

namespace org\camunda\php\sdk\command;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;
use org\camunda\php\sdk\logic\Camunda\Daemon;
use org\camunda\php\sdk\logic\Camunda\ProcessInstance;

class test extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'camunda:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //echo 'test';
        //Redis::publish('test', json_encode(['a'=>1, 'b'=>2]));

        /*
        Redis::psubscribe('test', function ($message, $channel) {
            echo $message;
            echo $channel;
        });*/

        $d = new Daemon();
        $p = new ProcessInstance();
        // $r = $d->getMessageSubscription();
        // $r = $d->getSignalSubscription();


        // $ns = $d->getTopicMames();
        // $ts = $d->fetchAndLockExternalTask('abc', $ns);
        // foreach ($ts as $t) {
        //     $r = $d->completeExternalTask($t->id, 'abc', $t->extensionProperties);
        // }

        // $ts = $d->fetchTasks();
        // foreach($ts as $t){
        //     $id = $t->getId();
        //     $r = $d->completeTask($id, []);
        // }

        $b = uniqid();
       $r = $p->startProcessByMessage('MessageStartEvent01_Message', $b);

         print_r($r);


       
    }
}
