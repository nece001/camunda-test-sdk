<?php

namespace org\camunda\php\sdk\command;

use Exception;
use Illuminate\Console\Command;
use org\camunda\php\sdk\logic\Camunda\Daemon;

class CamundaTestService extends Command
{
    public static $results = [];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'camunda:TestService';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Service for message signal topic task';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        echo 'Service start .';
        while(true){
            $this->topic();
            $this->message();
            $this->signal();
            $this->task();

            sleep(5);
        }
    }

    protected function message(){
        $daemon = new Daemon();
        $messages = $daemon->getMessageSubscription();
        foreach($messages as $message){
            $processInstanceId = $message->getProcessInstanceId();
            if($processInstanceId){
                $processInstance = $daemon->fetchProcessInstanceById($processInstanceId);
                $businessKey = $processInstance->getBusinessKey();
                $messageName = $message->getEventName();
                $daemon->sendMessage($businessKey, $messageName, $processInstanceId);
                echo 'Send Message: '.$messageName, PHP_EOL;
            }
        }
    }

    protected function signal(){
        $daemon = new Daemon();
        $signals = $daemon->getSignalSubscription();
        foreach($signals as $signal){
            $processInstanceId = $signal->getProcessInstanceId();
            if ($processInstanceId) {
                $signalName = $signal->getEventName();
                $daemon->sendSignal($signalName);
                echo 'Send Signal: '.$signalName, PHP_EOL;
            }
        }
    }

    protected function topic(){
        $workerId = uniqid();
        $daemon = new Daemon();
        $topicNames = $daemon->getTopicMames();
        $tasks = $daemon->fetchAndLockExternalTask($workerId, $topicNames);
        foreach ($tasks as $task) {
            $daemon->completeExternalTask($task->id, $workerId, $task->extensionProperties);
            echo 'Complete External Task: '.$task->topicName, PHP_EOL;
        }
    }

    protected function task(){
        $daemon = new Daemon();
        $tasks = $daemon->fetchTasks();
        foreach($tasks as $task){
            $id = $task->getId();
            try{
                $daemon->completeTask($id, []);
                echo 'Complete Task: '.$task->getName(), PHP_EOL;
            }catch(Exception $e){
                echo $e->getMessage();
            }
        }
    }
}
