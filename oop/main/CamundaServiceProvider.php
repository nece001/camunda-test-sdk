<?php

namespace org\camunda\php\sdk;

use Encore\Admin\Layout\Content;
use Illuminate\Routing\Router;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class CamundaServiceProvider extends ServiceProvider
{
    /**
     * @var array
     */
    protected $commands = [
        // Console\AdminCommand::class,
        command\test::class,
        command\CamundaTestAudit::class,
        command\CamundaTestService::class,
        command\CamundaTestStart::class
    ];

    public function register()
    {
        
        $this->commands($this->commands);

    }
}