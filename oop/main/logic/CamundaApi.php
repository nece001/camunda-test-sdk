<?php
namespace org\camunda\php\sdk\logic;

use org\camunda\php\sdk\Api;

class CamundaApi
{
    protected $host;
    protected $camundaApi;

    public function __construct()
    {
        $this->host = config('services.camunda.host');
        $this->camundaApi = new Api($this->host);
    }

    protected function formatTimestamp(int $timestamp){
        $timestamp = strtotime('-8 hour', $timestamp); // tomcat 的 java时区问题
        return date('Y-m-d\TH:i:s', $timestamp).'.000+0000';
    }

    protected function formatTimestampUrlEncode(int $timestamp){
        return urlencode($this->formatTimestamp($timestamp));
    }
}
