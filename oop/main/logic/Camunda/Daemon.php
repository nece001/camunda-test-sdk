<?php
namespace org\camunda\php\sdk\logic\Camunda;

use Exception;
use org\camunda\php\sdk\logic\CamundaApi;
use org\camunda\php\sdk\entity\request\HistoricProcessInstanceRequest;
use org\camunda\php\sdk\entity\request\HistoricActivityInstanceRequest;
use org\camunda\php\sdk\entity\request\HistoricVariableInstanceRequest;
use org\camunda\php\sdk\entity\request\MessageSubscriptionRequest;
use org\camunda\php\sdk\entity\request\ExternalTaskRequest;
use org\camunda\php\sdk\entity\request\TaskRequest;
use org\camunda\php\sdk\entity\request\MessageRequest;

class Daemon extends CamundaApi
{

    public function logCompletedProcessInstances($dir, $start_time, $count=10){
        $counter = 0;
        $processInstances = $this->fetchCompletedProcessInstances($start_time, $count);
        foreach($processInstances as $processInstance){
            $counter++;
            $this->writeLog($dir, $processInstance);
        }
        return $counter;
    }

    public function processInstanceIsCompletedById($id){
        $request = new HistoricProcessInstanceRequest();
        $request->setProcessInstanceId($id);
        
        $instances = $this->camundaApi->history->getProcessInstances($request);
        if($instances){
            foreach($instances as $instance){
                if($instance->getId() == $id && $instance->state == "COMPLETED"){
                    return true;
                }
            }
        }else{
            return true;
        }
        return false;
    }

    public function fetchProcessInstanceById($id){
        return $this->camundaApi->processInstance->getInstance($id);
    }

    public function fetchCompletedProcessInstances($start_time, $count=10){
        $request = new HistoricProcessInstanceRequest();
        $request->setFinishedAfter($this->formatTimestampUrlEncode($start_time));
        $request->setMaxResults($count);
        $request->setSortBy('endTime');
        $request->setSortOrder('desc');
        $request->active = 'false';
        
        return $this->camundaApi->history->getProcessInstances($request);
    }

    public function fetchCompletedProcessInstancesByIds($processInstanceIds){
        $request = new HistoricProcessInstanceRequest();
        $request->processInstanceIds = implode(',', $processInstanceIds);
        $request->active = 'false';
        $request->completed = 'true';
        
        return $this->camundaApi->history->getProcessInstances($request);
    }

    public function fetchHistoryActivityInstances($processInstanceId, $offset=0){
        $request = new HistoricActivityInstanceRequest();
        $request->setSortBy('occurrence');
        $request->setSortOrder('asc');
        $request->setProcessInstanceId($processInstanceId);
        $request->setFirstResult($offset);

        $activityInstances = $this->camundaApi->history->getActivityInstances($request);
        $variables = $this->fetchHistoryVariableInstances($processInstanceId);

        foreach($activityInstances as $activity){
            $activityName = $activity->getActivityName();
            foreach($variables as $variable){
                $var_name = $variable->getName();
                $hold = '{'.$var_name.'}';
                if(false !== strpos($activityName, $hold)){
                    $activityName = str_replace(array('$'.$hold, '#'.$hold), $variable->getValue(), $activityName);
                    $activity->setActivityName($activityName);
                }
            }
        }

        return $activityInstances;
    }

    public function fetchHistoryVariableInstances($processInstanceId){
        $request = new HistoricVariableInstanceRequest();
        $request->setProcessInstanceId($processInstanceId);

        return $this->camundaApi->history->getVariableInstances($request);
    }

    public function writeLog($dir, $processInstance){
        $processInstanceId = $processInstance->getId();
        $processDefinitionName = $processInstance->processDefinitionName;
        $activityInstances = $this->fetchHistoryActivityInstances($processInstanceId);
        $log = [];
        $log[] = $processDefinitionName;
        $filename = $dir .'/'.$processDefinitionName.'_'.$processInstanceId.'.log';
        foreach($activityInstances as $instance){
            $log[] = $instance->getActivityName();
        }

        file_put_contents($filename, implode(PHP_EOL, $log));
    }

    public function getMessageSubscription(){
        $request = new MessageSubscriptionRequest();
        $request->setEventType('message');

        return $this->camundaApi->messageSubscription->getList($request);
    }

    public function getSignalSubscription(){
        $request = new MessageSubscriptionRequest();
        $request->setEventType('signal');

        return $this->camundaApi->messageSubscription->getList($request);
    }

    public function sendMessage($businessKey, $messageName, $processInstanceId){
        $request = new MessageRequest();
        $request->setBusinessKey($businessKey);
        $request->setMessageName($messageName);
        $request->setProcessInstanceId($processInstanceId);
        return $this->camundaApi->messageSubscription->deliverMessage($request);
    }

    public function sendSignal($signalName){
        $request = new MessageSubscriptionRequest();
        $request->name = $signalName;
        return $this->camundaApi->messageSubscription->deliverSignal($request);
    }

    public function getTopicMames(){
        $request = new ExternalTaskRequest();
        $request->withUnlockedTasks = true;

        return $this->camundaApi->externalTask->getTopicMames($request);
    }

    public function fetchAndLockExternalTask($workerId, $topics){
        $topics_data = [];
        foreach($topics as $topic){
            $topics_data[] = [
                'topicName'=>$topic,
                'lockDuration'=>10000,
                'includeExtensionProperties'=>true
            ];
        }

        $request = new ExternalTaskRequest();
        $request->workerId = $workerId;
        $request->maxTasks = 20;
        $request->usePriority = true;
        $request->topics = $topics_data;

        return $this->camundaApi->externalTask->fetchAndLock($request);
    }

    public function completeExternalTask($task_id, $workerId, $variables){
        $vars = [];
        foreach($variables as $name=>$var){
            $vars[$name] = ['value'=>$var];
        }

        $request = new ExternalTaskRequest();
        $request->workerId = $workerId;
        $request->variables = $vars;

        return $this->camundaApi->externalTask->complete($task_id, $request);
    }

    public function fetchTasks(){
        $request = new TaskRequest();

        return $this->camundaApi->task->getTasks($request);
    }

    public function completeTask($task_id, $variables){
        $request = new TaskRequest();
        $request->setVariables($variables);

        $this->camundaApi->task->completeTask($task_id, $request);
    }


    public function deploy($files){
        $url = $this->host.'/deployment/create';
        $data = array(
			'deployment-name'=>'camunda-test-project', // 根据部署的名称，可以获取部署列表
			'enable-duplicate-filtering'=>false,
			//'deploy-changed-only'=>'',
			'deployment-source'=>'process application',
			//'tenant-id'=>''
        );
        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_NOBODY, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_AUTOREFERER, true);
	    if($files){
			foreach($files as $file){
				$info = pathinfo($file);
				$name = $info['basename'];
				$mime = $this->getFileMimeType($file);
				$data[$name] = new \CURLFile($file, $mime, $name);
			}

			if (class_exists('\CURLFile')) {
				curl_setopt($curl, CURLOPT_SAFE_UPLOAD, true);
			} else {
				if (defined('CURLOPT_SAFE_UPLOAD')) {
					curl_setopt($curl, CURLOPT_SAFE_UPLOAD, false);
				}
			}
		}

		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('content-type:multipart/form-data'));
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        $json = curl_exec($curl);
        if(curl_getinfo($curl, CURLINFO_HTTP_CODE) != '200'){
            $e = json_decode($json);
            throw new Exception($e->message);
        }
    }

    private function getFileMimeType($fileName)
    {
        if (phpversion() >= '5.3.0') 
        {    
            $finfo = finfo_open(FILEINFO_MIME);
            $mimeType = finfo_file($finfo, $fileName);
            finfo_close($finfo);
        }
        else 
        {
            $mimeType = mime_content_type($fileName);
        }    
        return $mimeType;
    }
}
