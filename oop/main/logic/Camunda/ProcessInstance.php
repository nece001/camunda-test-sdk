<?php
namespace org\camunda\php\sdk\logic\Camunda;

use org\camunda\php\sdk\logic\CamundaApi;
use org\camunda\php\sdk\entity\request\ProcessDefinitionRequest;
use org\camunda\php\sdk\entity\request\ProcessInstanceRequest;
use org\camunda\php\sdk\entity\request\MessageRequest;

class ProcessInstance extends CamundaApi
{
    private $key;
    private $businessKey;
    private $id;

    public function startProcess($key, $businessKey, $variables=null){
        $request = new ProcessDefinitionRequest();
        $request->setBusinessKey($businessKey);
        if($variables){
            $request->setVariables($variables);
        }
        

        $instance = $this->camundaApi->processDefinition->startInstanceByKey($key, $request);

        $this->key = $key;
        $this->businessKey = $businessKey;
        $this->id = $instance->getId();

        return $instance;
    }

    public function startProcessByMessage($messageName, $businessKey){
        $request = new MessageRequest();
        $request->setBusinessKey($businessKey);
        $request->setMessageName($messageName);

        try{
            $this->camundaApi->message->deliverMessage($request);

            $pRequest = new ProcessInstanceRequest();
            $pRequest->setBusinessKey($businessKey);
            $instances = (array)$this->camundaApi->processInstance->getInstances($pRequest);
            if($instances){
                return array_shift($instances);
            }
            return null;
        }catch(\Exception $e){
            echo "Exception     ".$e->getMessage();
            return false;
        }

    }

    public function messageCorrelate($messageName){
        $request = new MessageRequest();
        $request->setBusinessKey($this->businessKey);
        $request->setMessageName($messageName);

        try{
            $this->camundaApi->message->deliverMessage($request);
            return true;
        }catch(\Exception $e){
            echo "Exception     ".$e->getMessage();
            return false;
        }
    }
}