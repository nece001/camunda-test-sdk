<?php
/**
 * Created by IntelliJ IDEA.
 * User: hentschel
 * Date: 30.10.13
 * Time: 08:46
 * To change this template use File | Settings | File Templates.
 */

namespace org\camunda\php\sdk\service;

use Exception;
use org\camunda\php\sdk\entity\request\MessageSubscriptionRequest;
use org\camunda\php\sdk\entity\request\MessageRequest;
use org\camunda\php\sdk\entity\response\MessageSubscription;

class MessageSubscriptionService extends RequestService
{
    public function getList(MessageSubscriptionRequest $request) {
        $this->setRequestUrl('/event-subscription');
        $this->setRequestMethod('GET');
        $this->setRequestObject($request);
    
        try {
            $prepare = $this->execute();
            $response = array();
            foreach ($prepare AS $index => $data) {
              $MessageSubscription = new MessageSubscription();
              $response['MessageSubscription_' . $index] = $MessageSubscription->cast($data);
            }
            return (object)$response;
        } catch (Exception $e) {
          throw $e;
        }
      }

      public function deliverMessage(MessageRequest $request) {
        $this->setRequestUrl('/message');
        $this->setRequestMethod('POST');
        $this->setRequestObject($request);
    
        try {
          $this->execute();
        } catch (Exception $e) {
          throw $e;
        }
      }

      public function deliverSignal(MessageSubscriptionRequest $request) {
        $this->setRequestUrl('/signal');
        $this->setRequestMethod('POST');
        $this->setRequestObject($request);
    
        try {
          $this->execute();
        } catch (Exception $e) {
          throw $e;
        }
      }
}