<?php
namespace org\camunda\php\sdk\service;

use Exception;
use org\camunda\php\sdk\entity\request\ExternalTaskRequest;
use org\camunda\php\sdk\entity\response\ExternalTaskResponse;

class ExternalTaskService extends RequestService
{
    public function getTopicMames(ExternalTaskRequest $request){
        $this->setRequestUrl('/external-task/topic-names');
        $this->setRequestMethod('GET');
        $this->setRequestObject($request);

        try {
            return $this->execute();
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function fetchAndLock(ExternalTaskRequest $request)
    {
        $this->setRequestUrl('/external-task/fetchAndLock');
        $this->setRequestMethod('POST');
        $this->setRequestObject($request);

        try {
            $data = $this->execute();
            $items = [];
            foreach($data as $item){
                $response = new ExternalTaskResponse();
                $response->cast($item);
                $items[] = $response;
            }
            return $items;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function complete($id, ExternalTaskRequest $request){
        $url = '/external-task/'.$id.'/complete';
        $this->setRequestUrl($url);
        $this->setRequestMethod('POST');
        $this->setRequestObject($request);

        try {
            $this->execute();
        } catch (Exception $e) {
            throw $e;
        }
    }
}
